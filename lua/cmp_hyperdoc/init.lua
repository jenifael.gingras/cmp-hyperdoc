local source = {}

function source:get_trigger_characters()
    return { '(' }
end

--Invoke completion (required)
--@param params cmp.SourceCompletionApiParams
--@param callback fun(reponse: lsp.CompletionResponse|nil)
function source:complete(params, callback)
    local zet_path = params.option.zettelkasten_path
    local candidates = {}
    for f in vim.fs.dir(zet_path) do
        table.insert(candidates, { label = ':doc:`' .. f .. '`' })
    end
    callback(candidates)
end

return source
