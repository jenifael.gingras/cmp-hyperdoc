# cmp-hyperdoc

## Setup

```lua
require'cmp'.setup {
  sources = {
    { name = 'hyperdoc' }
  }
}
```


## Configuration

The below source configuration options are available. To set any of these options, do:

```lua
cmp.setup({
  sources = {
    {
      name = 'hyperdoc',
      option = {
        -- Options go into this table
        zettelkasten_path = "/home/user/Zettelkasten"
      },
    },
  },
})
```

### zettelkasten_path

_Default:_ ``

Folder to search for Zettelkasten note to link.
